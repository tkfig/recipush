<?php
	// @ error reporting setting  (  modify as needed )
	ini_set("display_errors", 1);
	error_reporting(E_ALL);
	
	//@ validate inclusion
	define('VALID_ACL_',		true);

	//@ load dependency files
	require('login.config.php');
	require('db.php');
	require('topscores.class.php');

	$hotRecepiesPercent = 25;
	$topscores = new TopScores;
	$message = $topscores->refresh($hotRecepiesPercent);
	unset($topscores);
?>