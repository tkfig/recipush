<?php
	//@ validate inclusion
	if(!defined('VALID_ACL_')) {
		exit('direct access is not allowed.');
	}	
	
	function DbFetch($result) {
		$array = array();
		
		if($result instanceof mysqli_stmt)
		{
			$result->store_result();
			
			$variables = array();
			$data = array();
			$meta = $result->result_metadata();
			
			while($field = $meta->fetch_field())
				$variables[] = &$data[$field->name]; // pass by reference
			
			call_user_func_array(array($result, 'bind_result'), $variables);
			
			$i=0;
			while($result->fetch())
			{
				$array[$i] = array();
				foreach($data as $k=>$v)
					$array[$i][$k] = stripslashes($v);
				$i++;
			}
		}
		elseif($result instanceof mysqli_result)
		{
			while($row = $result->fetch_assoc())
				$array[] = $row;
		}
		
		return $array;
	}

	function DbFetchArray($result) {
		$array = array();
		
		if($result instanceof mysqli_stmt)
		{
			$result->store_result();
			
			$variables = array();
			$data = array();
			$meta = $result->result_metadata();
			
			while($field = $meta->fetch_field())
				$variables[] = &$data[$field->name]; // pass by reference
			
			call_user_func_array(array($result, 'bind_result'), $variables);
			
			$i=0;
			while($result->fetch())
			{
				$array[$i] = array();
				foreach($data as $k=>$v)
					$array[$i][] = stripslashes($v);
				$i++;
			}
		}
		elseif($result instanceof mysqli_result)
		{
			while($row = $result->fetch_assoc())
				$array[] = $row;
		}
		
		return $array;
	}
?>