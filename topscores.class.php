<?php
//@ validate inclusion
if(!defined('VALID_ACL_')) exit('direct access is not allowed.');

class TopScores
{	
	private $_hotRecipesNumber;

	public function refresh($hotRecepiesPercent) {
		$recipesNumber = $this->countRecipes();
		$this->_hotRecipesNumber = (int)($recipesNumber*$hotRecepiesPercent/100);
		$this->dropOldTable();
		$this->createNewTable();
		$this->fillNewTable();
		$this->replaceTables();
		$this->dropOldTable();
	}
	
	public function countRecipes() {
		global $db_config;
		$mysqli = new mysqli($db_config['server'],$db_config['user'],$db_config['pass'], $db_config['name']);
		if (mysqli_connect_errno()) return false;

		$stmt = $mysqli->prepare(
			"SELECT 	COUNT(*) as cnt " .
			"FROM 		recipes;"
		);
		
		$stmt->execute();

		$result = DbFetch($stmt);
		$stmt->close();
		$mysqli->close();
		if (!$result || !$result[0]) return 0;

		return $result[0]['cnt'];;
	}

	private function createNewTable() {
		global $db_config; 
		$mysqli = new mysqli($db_config['server'],$db_config['user'],$db_config['pass'], $db_config['name']);
		if (mysqli_connect_errno()) return false;

		//Create new table
		$stmt = $mysqli->prepare(
			"CREATE TABLE hot_recipes_new LIKE hot_recipes;"
		);
		$stmt->execute();
		$stmt->close();
		$mysqli->commit();
		$mysqli->close();

		return true;
	}

	private function dropOldTable() {
		global $db_config; 
		$mysqli = new mysqli($db_config['server'],$db_config['user'],$db_config['pass'], $db_config['name']);
		if (mysqli_connect_errno()) return false;

		$stmt = $mysqli->prepare(
			"DROP TABLE IF EXISTS hot_recipes_new;"
		);
		$stmt->execute();
		$stmt->close();
		$mysqli->commit();

		$stmt = $mysqli->prepare(
			"DROP TABLE IF EXISTS hot_recipes_old;"
		);
		$stmt->execute();
		$stmt->close();
		$mysqli->commit();

		$mysqli->close();

		return true;
	}

	private function fillNewTable() {
		global $db_config; 
		$mysqli = new mysqli($db_config['server'],$db_config['user'],$db_config['pass'], $db_config['name']);
		if (mysqli_connect_errno()) return false;

		$stmt = $mysqli->prepare(
			"INSERT INTO hot_recipes_new (r_id)
				SELECT r_id
				FROM recipes
				ORDER BY r_score DESC
				LIMIT ?;"
		);
		$stmt->bind_param("i", 
			$this->_hotRecipesNumber
		);

		$stmt->execute();
		$stmt->close();
		$mysqli->commit();
		$mysqli->close();

		return true;
	}
	
	private function replaceTables() {
		global $db_config; 
		$mysqli = new mysqli($db_config['server'],$db_config['user'],$db_config['pass'], $db_config['name']);
		if (mysqli_connect_errno()) return false;

		//Create new table
		$stmt = $mysqli->prepare(
			"RENAME TABLE hot_recipes TO hot_recipes_old, hot_recipes_new TO hot_recipes;"
		);
		$stmt->execute();
		$stmt->close();
		$mysqli->commit();
		$mysqli->close();

		return true;
	}
}
?>